// on importe fs et path qui sont des modules du core de NodeJS
const fs = require('fs');
const path = require('path');
// on importe fast-csv qui permet de parser un fichier csv
const csv = require('fast-csv');
// on importe mongoose ODM pour MongoDB
const mongoose = require('mongoose');
// on se connecte au serveur
const URI = 'mongodb+srv://dekpo:qi08xn6@cluster0.i87hs.mongodb.net/postal_codes?retryWrites=true&w=majority';
mongoose.connect(URI,{ useUnifiedTopology: true, useNewUrlParser: true },(error) => {
    if (error) console.log('Error',error);
    console.log("Connected to MongoDB !!!");
});
// on définit un model basé sur un schéma en fonction des champs du fichiers csv
const mySchema = new mongoose.Schema({
    Nom_commune: String,
    Code_postal: String,
    coordonnees_gps: String
});
const myModel = mongoose.model('postal_codes',mySchema,'france');
// on crée un fusible pour faire des tests avant de lancer le traitement complet des 39192 lignes lol
let fuse = 0;
// on traite le fichier csv
fs.createReadStream(path.resolve(__dirname, '', 'laposte_hexasmal.csv'))
    .pipe(csv.parse({ headers: true,delimiter: ';' }))
    .on('error', error => console.error(error))
    .on('data', row => {
        // si notre fusible atteint une limite on stoppe le process
        // if (fuse==3) process.exit();
        console.log(row);
        // la création de mon nouveau model
        const postal_code = new myModel( row );
        // sauvegarde de mon model
        postal_code.save();
        // on incrémente le fusible
        fuse++;
    })
    .on('end', rowCount => console.log(`Parsed ${rowCount} rows`));
